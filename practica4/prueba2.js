
function por2Promesa(n){

    let prom = new Promise(function(resolve,reject){
	setTimeout(function(){
	    let res = n+1;
	    if(res==4){
		resolve(0);
	    }
	    else{
		reject(1000);
	    }
	},5000);
    }
    );

    return prom;
}

let resprom=por2Promesa(3);
resprom.then(function(a){
    console.log("El resultado es "+a);
}).catch(function(b){
    console.log("Ha terminado con codigo error "+b);
});

console.log("El codigo continua por aqui");
