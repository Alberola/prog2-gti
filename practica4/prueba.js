
function por2Promesa(n){

    let prom = new Promise(function(resolve,reject){

	setTimeout(function(){
	    resolve(n*2);
	},5000);
	
    }
    );

    return prom;
}

let resprom=por2Promesa(3);
resprom.then(function(a){
    console.log("El resultado es correcto y es "+a);
});

console.log("El codigo continua por aqui");
