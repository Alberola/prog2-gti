function sumarConPromesa(n,m){

    let promesa = new Promise(function(resolve,reject){
    
	setTimeout(function(){
	    let res = n+m;
	    if(res==7){
		resolve(res);
	    }
	    else{
		reject("Fallo realizando la suma");
	    }
	},3000);

	
    });//promesa


    return promesa;
    
}



let p = sumarConPromesa(3,4);

p.then(function(r){//r tendrá el valor que haya en res
    //esto se ejecuta cuando el resultado es satisfactorio
    console.log("El resultado es "+r);
}).catch(function(mensaje_error){
    console.log("ERROR: "+mensaje_error);
});

console.log("Este codigo continua por aquí");
