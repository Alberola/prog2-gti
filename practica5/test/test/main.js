
const Punto = require("../Punto.js")
let assert = require("assert")



describe("Pruebas de getters",function(){

it(" should return 4 ",function(done){

  let p1 = new Punto(2,4);
  assert.equal(p1.getY(),4);
  done();

});//it

it(" should return -1 ", function(){

  let p1 = new Punto(-1,-5);
  assert.equal(p1.getX(),-1);

});//it


});//describe
