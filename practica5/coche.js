
class Coche{

  constructor(ma,mo,anyo,puertas,caballos){
    this.marca = ma;
    this.modelo = mo;
    this.anyo = anyo;
    this.puertas = puertas;
    this.caballos = caballos;
  }

  calcularEdad(){
    return 2020-this.anyo;
  }

}

let c =new Coche("Ford","Kuga",2016,5,125);
/*console.log(c);
console.log(c.modelo);*/
console.log(c.calcularEdad());
