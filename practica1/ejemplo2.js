function f1(){
    return 1;
}

function f2(){
    return 2;
}

function f3(){
    setTimeout(function(){
	console.log("ahora realmente he terminado");
	return 3;
    }, 5000);
}

if(f1()==1){
    console.log("El resultado de f1 es correcto");
}

if(f2()==2){
    console.log("El resultado de f2 es correcto");
}

if(f3()==3){
    console.log("El resultado de f3 es correcto");
}
else{
    console.log("f3 ha petado");
}
