function f1(){
    return 1;
}

function f2(){
    return 2;
}

function f3(check){
    setTimeout(function(){
	console.log("ahora realmente he terminado");
	let resultado=3;
	check(resultado);
    }, 5000);
}

function comprobar(v){
    if(v==3) console.log("El resultado de f3 es correcto");
    else console.log("f3 ha petado");
}

if(f1()==1){
    console.log("El resultado de f1 es correcto");
}

if(f2()==2){
    console.log("El resultado de f2 es correcto");
}

f3(comprobar);

